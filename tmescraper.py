import argparse
import os
import sys
import urllib.request
import csv
from enum import Enum
from bs4 import BeautifulSoup
import pandas as pd
from colorama import Fore, Style


class StatusCodes(Enum):
    OK = 'OK'
    WARN = 'WARN'
    FAIL = 'FAILED'


def print_message(message, length, filler, newline):
    if newline is True:
        print('\t' + message.ljust(length, filler))
    else:
        print('\t' + message.ljust(length, filler), end='')


def print_status(status_code):
    text_code = ''
    if status_code == StatusCodes.OK:
        text_code = Fore.GREEN + str(
            status_code.value).center(6) + Style.RESET_ALL
    elif status_code == StatusCodes.FAIL:
        text_code = Fore.RED + str(
            status_code.value).center(6) + Style.RESET_ALL
    elif status_code == StatusCodes.WARN:
        text_code = Fore.YELLOW + str(
            status_code.value).center(6) + Style.RESET_ALL
    print(
        Style.DIM + '[' + Style.RESET_ALL + text_code +
        Style.DIM + ']' + Style.RESET_ALL)


def generate_safe_file_name(name):
    safe_chars = (' ', '.', '_', '-')  # array of safe characters
    return ''.join(c for c in name if c.isalnum() or c in safe_chars).rstrip()


def generate_output_path(name):
    # get script directory
    out_dir = os.path.dirname(__file__)
    # change output to whatever the output folder is
    return os.path.join(out_dir, 'output', generate_safe_file_name(name))


def generate_file_path(directory, filename):
    sequence = (generate_output_path(directory), '/', filename)
    return ''.join(sequence)


def parse_url(url, download_docs=False):
    # change these to suit your needs
    info_filename = 'info.txt'
    specs_filename = 'specs.txt'
    table_filename = 'specs.html'
    image_filename = 'image'  # no extension, we append the right one on save.

    print_message('Contacting website', 50, '.', False)
    try:
        # request data from the website and read it
        site_response = urllib.request.urlopen(url)
        response_data = site_response.read()
        print_status(StatusCodes.OK)

        # define an instance of Beautiful Soup using site data
        soup = BeautifulSoup(response_data, 'html.parser')

        # look for the producer and product
        print_message('Looking for product info', 50, '.', False)
        producer = soup.find(
            'h1', {'class': 'name'}).find(
            'span', {'class': 'producer_name'}).text.strip()
        product = soup.find('h2', {'class': 'subname'}).text.strip()
        if producer and product:
            print_status(StatusCodes.OK)
        else:
            print_status(StatusCodes.WARN)

        # look for the part number
        print_message('Looking for part number', 50, '.', False)
        part_number = soup.find(
            'td', {'class': 'value product-symbol pip-product-symbol'}).text
        if part_number:
            print_status(StatusCodes.OK)
        else:
            print_status(StatusCodes.WARN)

        # look for the specifications table
        print_message('Looking for specifications table', 50, '.', False)
        spec_table = soup.find(id='specification')
        if spec_table:
            print_status(StatusCodes.OK)
        else:
            print_status(StatusCodes.FAIL)

        # look for the product image
        print_message('Looking for image', 50, '.', False)
        photo_url = ''
        photo_box = soup.find(id='foto-box').find('img')
        if photo_box is not None:
            photo_url = photo_box['src']
            if photo_url is not None:
                print_status(StatusCodes.OK)
            else:
                print_status(StatusCodes.FAIL)
        else:
            print_status(StatusCodes.FAIL)

        # look for documentation links
        if download_docs:
            print_message('Looking for documentation links', 50, '.', False)
            documentation_table = soup.find(
                id='downloads').find_all('a', href=True)
            if documentation_table:
                print_status(StatusCodes.OK)
            else:
                print_status(StatusCodes.FAIL)

        # check to see if output directory exists, create it if it does not
        print_message('Checking if output path does not exist', 50, '.', False)
        if not os.path.exists(generate_output_path(part_number)):
            print_status(StatusCodes.OK)
            print_message('Creating directory', 50, '.', False)
            os.makedirs(generate_output_path(part_number))
            print_status(StatusCodes.OK)
        else:
            print_status(StatusCodes.WARN)

        # write the product info
        print_message('Writing info as text', 50, '.', False)
        with open(
                generate_file_path(
                    part_number, info_filename), 'w') as info_file:
            if producer is not None:
                info_file.write('Producer: ' + producer)
                info_file.write('\n')
            else:
                info_file.write('ERR_PRODUCER_NOT_FOUND')
            if product is not None:
                info_file.write(product)
                info_file.write('\n')
            else:
                info_file.write('ERR_PRODUCT_NOT_FOUND')
        print_status(StatusCodes.OK)

        # parse the specifications table and write the files
        if spec_table is not None:
            print_message('Parsing specifications table', 50, '.', False)
            spec_table_rows = []
            for row in spec_table.find_all('tr'):
                spec_table_rows.append(
                    [val.text.strip() for val in row.find_all('td')])
            print_status(StatusCodes.OK)
            print_message('Writing specifications as text', 50, '.', False)
            with open(
                    generate_file_path(
                        part_number, specs_filename), 'w',
                    encoding='utf8') as specs_file:
                writer = csv.writer(specs_file, delimiter='\t')
                writer.writerows(row for row in spec_table_rows if row)
            print_status(StatusCodes.OK)
            print_message('Writing specifications as HTML', 50, '.', False)
            pandas_data = pd.read_csv(
                generate_file_path(
                    part_number, specs_filename),
                delimiter='\t', header=None, usecols=[0, 1])
            pandas_data.to_html(
                generate_file_path(part_number, table_filename),
                header=False, index=False, border=0)
            print_status(StatusCodes.OK)

        # extract the image url extension if possible
        if photo_url is not None:
            image_url_split = photo_url.rsplit('.', 1)
            image_url_ext = ''
            if len(image_url_split) > 1:
                image_url_ext = '.' + image_url_split[len(image_url_split) - 1]
            # write the product image to file
            print_message('Saving product image', 50, '.', False)
            try:
                urllib.request.urlretrieve(
                    photo_url, generate_file_path(
                        part_number, image_filename + image_url_ext))
                print_status(StatusCodes.OK)
            except Exception as img_exception:
                print_status(StatusCodes.FAIL)

        # download the documentation if possible
        if download_docs:
            if documentation_table is not None and len(
                    documentation_table) > 0:
                print_message('Downloading documentation', 50, '.', False)
                for doc in documentation_table:
                    doc_url = 'https://www.tme.eu' + str(doc['href'])
                    # sanitize documentation filename
                    doc_safe_url = 'https://www.tme.eu' + urllib.parse.quote(
                        str(doc['href']))
                    doc_url_split = doc_url.rsplit('/', 1)
                    doc_filename = doc_url_split[len(doc_url_split) - 1]
                    # build the path to the documentation folder
                    doc_folder = os.path.join(
                        generate_output_path(part_number), 'docs')
                    # create documentation folder
                    if not os.path.exists(doc_folder):
                        os.makedirs(doc_folder)
                    urllib.request.urlretrieve(
                        doc_safe_url, os.path.join(doc_folder, doc_filename))
                print_status(StatusCodes.OK)

    except Exception as general_exception:
        # print exception details
        print('\n' + sys.exc_info()[0])


def parse_list(file, download_docs=False):
    # open the file and parse each line
    urls = []
    with open(str(file)) as url_file:
        urls = url_file.read().splitlines()
    for i, val in enumerate(urls):
        print('Parsing item ' + str(i + 1) + '/' + str(len(urls)))
        parse_url(val, download_docs)


# create command line arguments
parser_description = 'A script to scrape ' + \
    'https://www.tme.eu for product specifications.'
parser = argparse.ArgumentParser(description=parser_description)
# make the options mutually exclusive and required
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('-u', '--url', help='the url to scrape')
group.add_argument('-l', '--list', help='the path to the url list to scrape')
# optional flags
parser.add_argument('-d', '--docs', action='store_true',
                    help='enable downloading of documentation')
# parse the arguments so we can use them
args = parser.parse_args()

if __name__ == '__main__':
    if args.url:
        parse_url(args.url, args.docs)
    elif args.list:
        parse_list(args.list, args.docs)
