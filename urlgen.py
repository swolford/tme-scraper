import argparse


def generate_url(part_number, selected_region='gb'):
    sequence = (
        'https://www.tme.eu/{0}/details/{1}'.format(
            selected_region, part_number), '\n')
    return ''.join(sequence)


def generate_urls(input_path, output_path, selected_region='gb'):
    items = []
    # read the input file to an array of lines
    with open(str(input_path)) as input_file:
        items = input_file.read().splitlines()
    # generate a link to each product
    with open(str(output_path), 'w') as output_file:
        for i, val in enumerate(items):
            print('Generating item ' + str(i + 1) + ' of ' + str(len(items)))
            output_file.write(generate_url(val.lower(), selected_region))


# create command line arguments
parser_description = 'Generates links to products on https://www.tme.eu.'
parser = argparse.ArgumentParser(description=parser_description)
parser.add_argument('input',
                    help='the path to the list of items to generate links for')
parser.add_argument('output', help='the path of the generated links file')
parser.add_argument('-r', '--region',
                    help='the website region to scrape. default is "gb"')
args = parser.parse_args()

if __name__ == '__main__':
    if args.region:
        generate_urls(args.input, args.output, args.region)
    else:
        generate_urls(args.input, args.output)
