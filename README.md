# tme-scraper  
  
A Python script to scrape the contents of product pages on [tme.eu](https://www.tme.eu/) to make it easier for resellers to create product listings and provide information about their products.  
  
**Current Version:** 1.1.1  
  
## Features
  
* Downloading of basic product information (**ON** by default)
* Downloading of product specifications (**ON** by default)
* Downloading of product image (**ON** by default)
* Downloading of documentation (**OFF** by default)
* Generating a plain HTML table of specifications (**ON** by default)

## Dependencies  
  
* [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/)
* [colorama](https://pypi.python.org/pypi/colorama#downloads)  
* [pandas](http://pandas.pydata.org)
    
## Usage  
  
To scrape information on a single item:  
  
    python tmescraper.py --url https://www.tme.eu/gb/details/fyl-5014bgc1c/tht-leds-5mm/foryard/
  
Or to scrape a list of items:  
  
    python tmescraper.py --list products.txt
  
The scraped info will be put in a directory in the script location called `output` with the following directory structure:  
  
    /output
        /PRODUCT_1/
            image.jpg
            info.txt
            specs.html
            specs.txt
        /PRODUCT_2/
            image.jpg
            info.txt
            specs.html
            specs.txt
        ...
  
If the `--docs` argument is passed, each output directory will contain a `docs` folder with all of the documentation saved with their respective filenames.  
  
**NOTE:** The script will warn you if the product directory already exists and overwrite the existing files. Overwriting is the default function in case the product listing gets updated. Be sure to back up any existing products that you don't want overwritten prior to running the script!  
  
## urlgen  
  
There is an additional script called `urlgen.py` that generates a list of simplified product URLs from a list of product codes as input. It is primarily useful for those who have bulk ordered and only have the product codes on hand.  
  
Usage is extremely simple:  
  
    python urlgen.py --region nl codes.txt links.nl.txt
  
The script will spit out a simplified URL list for The Netherlands since [tme.eu](https://www.tme.eu/) has some pretty nice redirects in place. You can then use the generated list as input for the scraper.  

**NOTE:** If the `--region` argument is not specified, `gb` will be used as default. I've only tested with `gb` and `nl` but it should work for other regions.  
  
Sample output using `FYL-5014BGC1C` as input and the default region of `gb`: 
  
    https://www.tme.eu/gb/details/fyl-5014bgc1c
  
## To-Do
  
* Logging errors to file or console
* Better error handling
* Send a User Agent to identify the script
